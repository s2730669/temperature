package nl.utwente.di.temp;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class temp extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private converter c;
	
    public void init() throws ServletException {
        c = new converter();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

      response.setContentType("text/html");
      PrintWriter out = response.getWriter();
      String docType =
              "<!DOCTYPE HTML>\n";
      String title = "Temperature converter";
      out.println(docType +
              "<HTML>\n" +
              "<HEAD><TITLE>" + title + "</TITLE>" +
              "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
              "</HEAD>\n" +
              "<BODY BGCOLOR=\"#FDF5E6\">\n" +
              "<H1>" + title + "</H1>\n" +
              "  <P>Celsius temperature: " +
              request.getParameter("celsius") + "\n" +
              "  <P>Fahrenheit: " +
              Double.toString(c.CtoF(Double.parseDouble(request.getParameter("celsius")))) +
              "</BODY></HTML>");
  }
  

}
